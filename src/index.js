import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route} from "react-router-dom";
import Profile from "./Profile/Profile";
import Dialogs from "./Dialogs/Dialogs";
import Music from "./Music/Music";
import NavMenu from "./Nav_menu/Nav";
import Head from "./Head/Head";
import Todo from "./ToDoList/ToDoList";
import Weather from "./Weather/Weather";
import store from "./Redux/Redux-store";

const Test = (store) => {
    return (
        <BrowserRouter>
            <div className="full">
            <div className="flex-up">
                <Head/>
                <NavMenu/>
                <Route path='/profile' render={() => <Profile store={store}/>}/>
                <Route path='/dialogs' component={Dialogs}/>
                <Route path='/music' component={Music}/>
                <Route path='/todo' component={Todo}/>
                <Route path='/weather' component={Weather}/>
            </div>
            </div>
        </BrowserRouter>
    );
}


ReactDOM.render(<Test/>, document.getElementById("root"))

serviceWorker.unregister();