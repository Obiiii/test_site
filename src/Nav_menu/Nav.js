import React from 'react';
import "./Nav.css"
import Button from "@material-ui/core/Button";
// import Link from '@material-ui/core/Link';
import {Link} from 'react-router-dom'

const NavMenu = () => {
    return (
        <nav className="main-menu">
            <div>
                <Button variant="contained" color="primary"><Link color="inherit" to='/profile'> Профиль </Link> </Button>
            </div>
            <div>
                <Button variant="contained" color="primary"><Link color="inherit" to='/dialogs'> Сообщения </Link></Button>
            </div>
            <div>
                <Button variant="contained" color="primary"><Link color="inherit" to='/music'> Музыка </Link></Button>
            </div>
            <div>
                <Button variant="contained" color="primary"><Link color="inherit" to='/todo'> Список дел </Link></Button>
            </div>
            <div>
                <Button variant="contained" color="primary"><Link color="inherit" to='/weather'> Погода </Link></Button>
            </div>
        </nav>
    )
}

export default NavMenu