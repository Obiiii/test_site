import React, {Component} from 'react';
import "./Profile.css"
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {useState} from 'react'


const Profile = (props) => {

    const [text, setText] = useState()

    const changeTF = (event) => {
        this.setState({textup: event.target.value});
        props.store.dispatch({type: 'UPDATE_POST', newText: text})
    }
    const clickPost = () => {
        props.store.dispatch({type: 'ADD_POST'})
    }
    return (
        <div className="future">
            <div className={'profile-title'}>
                Avatar+desc
            </div>
            <div className="text-enter">
                <TextField
                    label="Enter post"
                    autoComplete="current-password"
                    onChange={(e) => this.changeTF(e)}
                />
                <Button variant="contained" color="primary" onClick={(e) => clickPost(e)}>Поделиться</Button>
                <br/>
            </div>
            <div>
                {props.store.posts((item, i) => <Card className={'post-card'} key={i}>
                    <CardContent className="card_content">
                        {item}
                        <div className="profile__date">{props.store.date[i]}</div>
                    </CardContent>
                </Card>)}
            </div>
        </div>)
}


export default Profile