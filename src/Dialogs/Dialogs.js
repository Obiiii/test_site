import React from 'react';
import "./Dialogs.css"
import {Link} from "react-router-dom";

const DialogsItem = (props) => {
    let path = "/dialogs/" + props.id;
    return <Link to={path}>{props.name}</Link>
}

const Dialogs = (props) => {

    return (
        <div className="dialog__back">
            <div className="dialog__list">
                <DialogsItem name="Vasja" id ="1"/>
                <DialogsItem name="Sveta" id ="2"/>
                <DialogsItem name="Janara" id ="3"/>
                <DialogsItem name="Nikita" id ="4"/>
            </div>
            <div className="dialog__content">
                <div>
                    Привет
                </div>
                <div>
                    Ты как?
                </div>
            </div>
        </div>
    )
}

export default Dialogs
