import React from 'react';
import "./Head.css"

const Head = () => {
    return (
        <div className="head-main">
            <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Gillette-Logo.svg/1280px-Gillette-Logo.svg.png" alt="Web_Logo"/>
        </div>
    )
}

export default Head