const ADD_POST = 'ADD_POST';
const UPDATE_POST ="UPDATE_POST";

let initialState = {
    textup: '',
    posts:[],
    date:[]
};

const profileReducer = (state=initialState,action) =>{
    switch (action.type) {
        case (ADD_POST):
            let prevposts = this.state.posts
            let nextposts = this.state.textup
            var date = new Date()
            let prevdate = this.state.date
            let datepost = [date.getFullYear(),':',date.getMonth(),':',date.getDay(),'   ',date.getHours(),':',date.getMinutes(),':',date.getSeconds()]
            this.setState({
                posts:[...prevposts, nextposts],
                date:[...prevdate,datepost]
            })
            return state;
        case (UPDATE_POST):
            state.textup = action.newText;
            return state;
        default:
            return state;
    }
}

export default profileReducer()