import React, {useState} from 'react';
import "./ToDoList.css"
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete"

const Todo = () => {
    const [todo, setTodo] = useState('')
    const [reactiveTodos, setReactiveTodos] = useState([])
    const handleChange = (event) => {
        setTodo(event.target.value)
    }

    const handleClick = (event) => {
        const todos = reactiveTodos
        const nextTodos = [...todos, todo]
        setReactiveTodos(nextTodos)
    }

    const handleDelete = (i) => {
        let todos = reactiveTodos
        todos = todos.filter((item, elI) => {
            return elI !== i
        })


        setReactiveTodos(todos)
    }

    return (
        <div className="b-todos todo__back">
            <h1 align="center">Список дел</h1>
            <div className="todos__main todo__main">
                <Input onChange={handleChange}/>
                <Button onClick={handleClick} id="todos__btn">Добавить</Button>
                <ul className={'todos__list'}>
                    {
                        reactiveTodos.map((item, i) => <li className={'todos__item'} key={i}>{item}
                            <button onClick={() => handleDelete(i)}><DeleteIcon/></button>
                        </li>)
                    }
                </ul>
            </div>
        </div>
    )
}
export default Todo