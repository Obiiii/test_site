import React, {Component} from 'react';
import "./Weather.css";
import Axios from 'axios';
import Button from "@material-ui/core/Button";
import Chart from 'react-apexcharts'


class Weather extends Component {
    constructor(props) {
        super(props);

        this.state = {
            options: {
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: ["now", "tomorrow", "tomorrow + 1", "tomorrow + 2", "tomorrow + 3"]
                },
                yaxis:{
                    min:-20,
                    max:20
                }
            },
            series: [
                {
                    name: "series-1",
                    data: []
                }
            ]
        };

        this.handleClick = this.handleClick.bind(this)
    }


    handleClick() {
        let keywet = '7a661889a18e208056285c3479a93d43'
        const proxy = "https://cors-anywhere.herokuapp.com/";
        const query = `${proxy}https://api.openweathermap.org/data/2.5/forecast?q=Smolensk&mode=json&APPID=${keywet}`;

        Axios
            .get(query, {
                headers: {
                    "x-requested-With": "XMLHttpRequest",
                    "X-CSRFToken": "example-of-custom-header"
                }
            })
            .then((response) => {
                    let weathone = [(response.data.list[0].main.temp - 275).toFixed(1), (response.data.list[1].main.temp - 275).toFixed(1), (response.data.list[2].main.temp - 275).toFixed(1), (response.data.list[3].main.temp - 275).toFixed(1), (response.data.list[4].main.temp - 275).toFixed(1)]
                    this.setState({
                        series: [
                            {
                                name: "series-1",
                                data: weathone
                            }
                        ]
                    })

                }
            )
    }


    render() {
        return (

            <div className='weath__main'>
                <Button variant="contained" color="primary" onClick={this.handleClick}>
                    Вывести температуру
                </Button>
                <div className="app">
                    <div className="row">
                        <div className="mixed-chart">
                            <Chart
                                options={this.state.options}
                                series={this.state.series}
                                type="line"
                                width="800"
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


/* class App extends Component {
 constructor(props) {
   super(props);

   this.state = {
     options: {
       chart: {
         id: "basic-bar"
       },
       xaxis: {
         categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
       }
     },
     series: [
       {
         name: "series-1",
         data: [30, 40, 45, 50, 49, 60, 70, 91]
       }
     ]
   };
 }

 render() {
   return (
     <div className="app">
       <div className="row">
         <div className="mixed-chart">
           <Chart
             options={this.state.options}
             series={this.state.series}
             type="bar"
             width="500"
           />
         </div>
       </div>
     </div>
   );
 }
}
*/
export default Weather